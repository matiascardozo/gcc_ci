# gcc_ci

## Proceso de integración contínua 


 Creación de archivo .gitlab-ci.yml en formato YAML

 Definición de las etapas o stages. 
*   Build   
*   Test    
*   Deploy

 Se definen las tareas o jobs para cada stage.
 Cada tarea pertenece a algún stage. Los stages se ejecutan en ese orden estrictamente. 

 
 * build
  
 La tarea build intenta instalar las dependencias y requerimientos necesarios para levantar el proyecto, pertenece a la etapa de Build y su resultado pertenece al ambiente de desarrollo

 * test 
  
La tarea test realiza las pruebas unitarias definidas en el proyecto y pertenece a la etapa Test

Los cambios en cualquier rama del proyecto ocasionan que estas dos tareas se ejecuten.


 * deploy_staging
 
La tarea deploy_staging envía el proyecto al ambiente de homologación, definido por una instancia de heroku. Se activa mediante los cambios en la rama master. 


 * deploy_prod

La tarea deploy_prod envía el proyecto al ambiente de producción a una instancia de Heroku, distinta al ambiente de Homologación. Se activa mediante la definición de tags

