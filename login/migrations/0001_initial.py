# Generated by Django 2.2.1 on 2019-05-12 17:33

from django.db import migrations
from django.contrib.auth.hashers import make_password


def create_user(apps, schema_editor):
    # We can't import the User model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    User = apps.get_model('auth', 'User')
    user = User(username='admin')
    user.is_staff = True
    user.is_superuser = True
    user.password = make_password('admin')
    user.save()
    


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0011_update_proxy_permissions') 
    ]

    operations = [
        migrations.RunPython(create_user),
    ]
