from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse

class UserModelTest(TestCase):

    def test_existe_usuario(self):
        User.objects.create(username='joe')
        u = User.objects.get(username='joe')
        self.assertIs(u.username == 'joe', True)


    def test_login_view(self):
        User.objects.create_user(username='joe', email="joe@asdf.com",password='joe')
        response = self.client.post(reverse('login'), {'username': 'joe', 'password': 'joe'}, follow=True)
        self.assertContains(response, 'Welcome')
